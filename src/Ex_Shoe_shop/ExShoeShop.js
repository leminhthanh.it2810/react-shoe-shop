import React, { Component } from "react";
import Cart from "./Cart.js";
import { dataShoe } from "./data_shoe.js";
import ListShoe from "./ListShoe.js";
export default class ExShoeShop extends Component {
  state = {
    listShoe: dataShoe,
    cart: [],
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });

    if (index === -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }

    this.setState({
      cart: cloneCart,
    });
  };
  render() {
    return (
      <div className="container">
        <h2>{this.props.children}</h2>
        <Cart
          cart={this.state.cart}
          onCartChange={(cart) => {
            const filteredCart = cart.filter((item) => item.soLuong > 0);
            this.setState({ cart: filteredCart });
          }}
        />
        <ListShoe
          handleAddToCart={this.handleAddToCart}
          list={this.state.listShoe}
        />
      </div>
    );
  }
}
