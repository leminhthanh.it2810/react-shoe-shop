import React, { Component } from "react";

export default class Cart extends Component {
  handleIncreaseQuantity = (item) => {
    let { cart } = this.props;
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((i) => i.id === item.id);
    cloneCart[index].soLuong++;
    this.setState({ cart: cloneCart });
  };

  handleDecreaseQuantity = (item) => {
    let { cart } = this.props;
    let cloneCart = [...cart];
    let index = cloneCart.findIndex((i) => i.id === item.id);
    if (cloneCart[index].soLuong > 1) {
      cloneCart[index].soLuong--;
    } else {
      cloneCart.splice(index, 1);
    }
    this.props.onCartChange(cloneCart);
  };
  
  handleRemoveFromCart = (item) => {
    let cloneCart = [...this.props.cart];
    let index = cloneCart.findIndex((cartItem) => {
      return cartItem.id === item.id;
    });
    if (index !== -1) {
      cloneCart.splice(index, 1);
    }
    this.props.onCartChange(cloneCart);
  };

  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <img src={item.image} alt="" style={{ width: 50 }} />
          </td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => this.handleDecreaseQuantity(item)}
            >
              -
            </button>
            <strong className="mx-3">{item.soLuong}</strong>
            <button
              className="btn btn-success"
              onClick={() => this.handleIncreaseQuantity(item)}
            >
              +
            </button>
          </td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => this.handleRemoveFromCart(item)}
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <th>ID </th>
            <th>name</th>
            <th>Price</th>
            <th>Img</th>
            <th>Quantity</th>
            <th>Thao tác</th>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
