import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name, price, description } = this.props.item;
    return (
      <div className="col-3 p-4">
        <div className="card ">
          <img src={image} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">
              Price: {price}
              <br />
              Description: {description}
            </p>
            <button onClick={() =>{this.props.handleOnclick(this.props.item)}} className="btn btn-primary">
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}
