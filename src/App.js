import logo from './logo.svg';
import './App.css';
import ExShoeShop from './Ex_Shoe_shop/ExShoeShop';

function App() {
  return (
    <div className="App">
      <ExShoeShop />
    </div>
  );
}

export default App;
